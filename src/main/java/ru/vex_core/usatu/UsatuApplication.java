package ru.vex_core.usatu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UsatuApplication {

	public static void main(String[] args) {
		SpringApplication.run(UsatuApplication.class, args);
	}

}
