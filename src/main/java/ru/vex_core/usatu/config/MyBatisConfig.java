package ru.vex_core.usatu.config;

import org.apache.ibatis.datasource.pooled.PooledDataSource;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.servlet.annotation.HandlesTypes;
import javax.sql.DataSource;

@Configuration
@EnableTransactionManagement
@MapperScan(value = "ru.vex_core.usatu.dao.mapper")
@PropertySource("classpath:database.properties")
public class MyBatisConfig {

    @Value("${datasource.url}")
    private String dbUrl;
    @Value("${datasource.username}")
    private String dbUser;
    @Value("${datasource.password}")
    private String dbPass;
    @Value("${datasource.driver}")
    private String dbDriver;

    @Value("${datasource.maxConnections}")
    private Integer maxActiveConnections;

    @Bean
    public DataSource dataSource() {
        PooledDataSource dataSource = new PooledDataSource(dbDriver, dbUrl, dbUser, dbPass);
        dataSource.setPoolMaximumActiveConnections(maxActiveConnections);

        org.apache.ibatis.logging.LogFactory.useLog4JLogging();
        return dataSource;
    }
}