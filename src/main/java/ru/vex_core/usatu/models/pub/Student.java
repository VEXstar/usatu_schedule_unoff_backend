package ru.vex_core.usatu.models.pub;

public class Student {
    protected String firstName;
    protected String lastName;
    protected String middleName;
    protected Integer id;
    protected Integer groupID;

    protected Group group;

    public Student() {
    }

    public Student(String firstName, String lastName, String middleName, Integer id, Integer groupID) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.middleName = middleName;
        this.id = id;
        this.groupID = groupID;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public Integer getId() {
        return id;
    }

    public Integer getGroupID() {
        return groupID;
    }
}
