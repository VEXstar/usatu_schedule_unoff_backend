package ru.vex_core.usatu.models.pub;

import java.util.List;

public class Exam {
    public enum typeOfExam {EXAM,CONSULTATION}
    protected Integer id;
    protected typeOfExam typeOfExam;
    protected String title;
    protected Integer[] groupsID;
    protected Integer teacherID;
    protected Integer auditoriesID;
    protected Long timestamp;
    protected Integer semID;

    protected List<Group> groups;
    protected Teacher teacher;
    protected Auditory auditory;

    public Integer getSemID() {
        return semID;
    }

    public Exam() {
    }

    public Exam(Integer id, Exam.typeOfExam typeOfExam, String title, Integer[] groupsID, Integer teacherID, Integer auditoriesID, Long timestamp, Integer semID) {
        this.id = id;
        this.typeOfExam = typeOfExam;
        this.title = title;
        this.groupsID = groupsID;
        this.teacherID = teacherID;
        this.auditoriesID = auditoriesID;
        this.timestamp = timestamp;
        this.semID = semID;
    }

    public void setGroupsID(Integer[] groupsID) {
        this.groupsID = groupsID;
    }

    public typeOfExam getTypeOfExam() {
        return typeOfExam;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public Integer getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public Integer[] getGroupsID() {
        return groupsID;
    }

    public Integer getTeacherID() {
        return teacherID;
    }

    public Integer getAuditoriesID() {
        return auditoriesID;
    }

    public Long getDate() {
        return timestamp;
    }
}
//