package ru.vex_core.usatu.models.pub;

import ru.vex_core.usatu.providers.ParseProvider;

public class Group {
    protected String title;
    protected int level;
    protected Integer id;
    protected int studCount;
    protected Integer facID;

    protected Faculty faculty;

    public Group() {
    }

    public Group(String title, int level, Integer id, int studCount, Integer facID) {
        this.title = title;
        this.level = level;
        this.id = id;
        this.studCount = studCount;
        this.facID = facID;
    }

    public Group(String title, Integer id,Integer facID, ParseProvider provider) {
        this.title = title;
        this.id = id;
        this.level = (int)provider.parse(title);
        this.facID = facID;

    }

    public String getTitle() {
        return title;
    }

    public int getLevel() {
        return level;
    }

    public Integer getId() {
        return id;
    }

    public int getStudCount() {
        return studCount;
    }

    public Integer getFacID() {
        return facID;
    }
}
