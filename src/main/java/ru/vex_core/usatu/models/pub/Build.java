package ru.vex_core.usatu.models.pub;
public class Build {
    protected Integer id;
    protected String title;
    protected String about;

    public Build(Integer id, String title, String about) {
        this.id = id;
        this.title = title;
        this.about = about;
    }

    public Build() {
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }
}
