package ru.vex_core.usatu.models.pub;

public class Teacher {

    protected String teacherName;
    protected Integer departmentID;
    protected Integer facID;
    protected Integer id;
    protected Integer score;


    protected Department department;
    protected Faculty faculty;

    public Teacher(String teacherName, Integer departmentID, Integer facID, Integer id, Integer score) {
        this.teacherName = teacherName;
        this.departmentID = departmentID;
        this.facID = facID;
        this.id = id;
        this.score = score;
    }

    public Teacher() {
    }

    public String getTeacherName() {
        return teacherName;
    }

    public Integer getDepartmentID() {
        return departmentID;
    }

    public Integer getFacID() {
        return facID;
    }

    public Integer getId() {
        return id;
    }

    public int getScore() {
        return score;
    }
}
