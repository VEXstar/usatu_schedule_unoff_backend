package ru.vex_core.usatu.models.pub;

import java.util.List;

public class Schedule {
    public enum typeOfLesson {LECTION,PRACTIC,LESSONandPRACTIC,LAB,LPL,CONSULTATION,PHYEDUCATION,ARMYBOOTS,OTHER}

    public String getTitle() {
        return title;
    }

    public Integer getId() {
        return id;
    }

    protected String title;
    protected Integer id;
    protected Integer[]  weeks;
    protected Integer teachersIDs;
    protected Integer[] groupsIds;
    protected Integer auditoryID;
    protected Schedule.typeOfLesson typeOfLesson;
    protected int dayOfWeek;
    protected int numOfDay;
    protected Integer semID;

    //Composite
    protected List<Group> groups;
    protected Teacher teacher;
    protected Auditory auditory;

    public Schedule() {
    }

    public Integer getSemID() {
        return semID;
    }

    public int getDayOfWeek() {
        return dayOfWeek;
    }

    public int getNumOfDay() {
        return numOfDay;
    }

    public Schedule(String title, Integer id, Integer[] weeks, Integer teachersIDs, Integer[] groupsIds, Integer auditoryID, Schedule.typeOfLesson typeOfLesson, int dayOfWeek, int numOfDay, Integer semID) {
        this.title = title;
        this.id = id;
        this.weeks = weeks;
        this.teachersIDs = teachersIDs;
        this.groupsIds = groupsIds;
        this.auditoryID = auditoryID;
        this.typeOfLesson = typeOfLesson;
        this.dayOfWeek = dayOfWeek;
        this.numOfDay = numOfDay;
        this.semID = semID;
    }

    public String getLessonName() {
        return title;
    }

    public Integer[] getWeeks() {
        return weeks;
    }

    public Integer getTeachersIDs() {
        return teachersIDs;
    }

    public Integer[] getGroupsIds() {
        return groupsIds;
    }

    public void setGroupsIds(Integer[] groupsIds) {
        this.groupsIds = groupsIds;
    }

    public Integer getAuditoryID() {
        return auditoryID;
    }

    public Schedule.typeOfLesson getTypeOfLesson() {
        return typeOfLesson;
    }
}
