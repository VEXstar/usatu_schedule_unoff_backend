package ru.vex_core.usatu.models.pub;


public class Auditory {
    protected Integer id;
    protected String name;
    protected int floor;
    protected Integer buildId;

    protected Build build;

    public Auditory() {
    }

    public Auditory(Integer id, String name, int floor, Integer buildId) {
        this.id = id;
        this.name = name;
        this.floor = floor;
        this.buildId = buildId;
    }


    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getFloor() {
        return floor;
    }

    public Integer getBuildID() {
        return buildId;
    }
}
