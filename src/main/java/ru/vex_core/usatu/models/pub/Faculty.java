package ru.vex_core.usatu.models.pub;

public class Faculty {
    protected String title;
    protected Integer id;
    protected String reqName;

    public Faculty(String title, Integer id, String reqName) {
        this.title = title;
        this.id = id;
        this.reqName = reqName;
    }

    public Faculty() {
    }

    public String getTitle() {
        return title;
    }

    public Integer getId() {
        return id;
    }

    public String getReqName() {
        return reqName;
    }

    public void setReqName(String reqName) {
        this.reqName = reqName;
    }
}
