package ru.vex_core.usatu.models.pub;

import java.util.concurrent.atomic.AtomicReference;

public class Debug {
    AtomicReference<String> status = new AtomicReference<>();
    long lastupdate;


    public void setStatus(String status) {
        lastupdate = System.currentTimeMillis();
        this.status.set(status);
    }
}
