package ru.vex_core.usatu.models.pub;

public class Department {
    protected String title;
    protected Integer id;
    protected Integer facID;


    protected Faculty faculty;

    public Department(String title, Integer id, Integer facID) {
        this.title = title;
        this.id = id;
        this.facID = facID;
    }

    public Department() {
    }

    public String getTitle() {
        return title;
    }

    public Integer getId() {
        return id;
    }

    public Integer getFacID() {
        return facID;
    }
}
