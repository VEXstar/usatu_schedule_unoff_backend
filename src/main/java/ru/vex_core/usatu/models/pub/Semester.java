package ru.vex_core.usatu.models.pub;

public class Semester {
    protected String title;
    protected int id;

    public Semester(int id,String title) {
        this.title = title;
        this.id = id;
    }

    public Semester() {
    }

    public String getTitle() {
        return title;
    }

    public int getId() {
        return id;
    }
}
