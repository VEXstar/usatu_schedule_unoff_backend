package ru.vex_core.usatu.models.response;

public class UniversalResponseModelForV2Api {
    public responseCodes getCode() {
        return code;
    }

    public enum responseCodes {ERROR,OK,FAIL_PARSE,NO_MATCH_FIELDS,SERVER_ERROR}
    private String errorMessage;
    private responseCodes code;
    private Object body;
    private Long totalTimeRequsert;
    private Long timeRequset;

    public UniversalResponseModelForV2Api(String errorMessage, responseCodes code, Object body, Long totalTimeRequsert, Long timeRequset) {
        this.errorMessage = errorMessage;
        this.code = code;
        this.body = body;
        this.totalTimeRequsert = totalTimeRequsert;
        this.timeRequset = timeRequset;
    }
}
