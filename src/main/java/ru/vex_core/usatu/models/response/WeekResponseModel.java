package ru.vex_core.usatu.models.response;

public class WeekResponseModel {
    private Integer week;

    public WeekResponseModel() {
    }

    public WeekResponseModel(Integer week) {
        this.week = week;
    }

    public Integer getWeek() {
        return week;
    }

    public void setWeek(Integer week) {
        this.week = week;
    }
}
