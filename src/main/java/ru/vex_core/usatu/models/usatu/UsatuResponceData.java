package ru.vex_core.usatu.models.usatu;

public class UsatuResponceData {
    private String mane;
    private String id;

    public UsatuResponceData(String mane, String id) {
        this.mane = mane;
        this.id = id;
    }

    public String getMane() {
        return mane;
    }

    public void setMane(String mane) {
        this.mane = mane;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
