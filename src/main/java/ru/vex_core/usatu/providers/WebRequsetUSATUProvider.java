package ru.vex_core.usatu.providers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import ru.vex_core.usatu.logic.web.AuthLogic;
import ru.vex_core.usatu.logic.web.HttpClient;

public class WebRequsetUSATUProvider {

    private AuthLogic authLogic;



    public WebRequsetUSATUProvider(String username, String password, String hostUri,String classAuth){
        authLogic = new AuthLogic(username,password,hostUri,classAuth);
    }


    public String getData(String method, String uri, MultiValueMap<String,String> auditionalHeaders, MultiValueMap<String,String> bodyData) throws InterruptedException {
        if(auditionalHeaders ==null)
            auditionalHeaders = new LinkedMultiValueMap<>();
        if(bodyData == null)
            bodyData = new LinkedMultiValueMap<>();

        ResponseEntity<String> stringResponseEntity = null;
        authLogic.subscribeTokensFor(auditionalHeaders,bodyData);

        if(method.toLowerCase().equals("post")){

            stringResponseEntity = HttpClient.doPost(uri, auditionalHeaders, bodyData);
        }
        else if(method.toLowerCase().equals("get")){
            stringResponseEntity = HttpClient.doGet(uri, auditionalHeaders, bodyData);
        }
        assert stringResponseEntity != null;
        if(stringResponseEntity.getStatusCode()== HttpStatus.OK){
            return stringResponseEntity.getBody();
        }


        return null;
    }
}
