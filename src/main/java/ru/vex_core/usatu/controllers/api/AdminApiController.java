package ru.vex_core.usatu.controllers.api;


import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.vex_core.usatu.models.response.UniversalResponseModelForV2Api;
import ru.vex_core.usatu.services.grabling.ScheuldeGrablingService;
import ru.vex_core.usatu.services.grabling.DataGrablingService;

@RestController
@EnableAutoConfiguration
@RequestMapping(value = "/admin/api", produces = "text/plain;charset=UTF-8")
public class AdminApiController {


    @Autowired
    DataGrablingService dataGrablingService;
    @Autowired
    ScheuldeGrablingService scheuldeGrablingService;


    @RequestMapping("/update")
    ResponseEntity<String> updGroups(String authtoken,String login)
    {
        if(authtoken.equals("deadKey")&&login.equals("admin")){
            Thread thread  = new Thread(()->{
                try {
                    dataGrablingService.updateData(true);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
            thread.start();
            return ResponseEntity.ok("OK");
        }
        return ResponseEntity.status(HttpStatus.FORBIDDEN).body("sorry, please check data");
    }
    @RequestMapping("/status")
    ResponseEntity<String> status(){
        UniversalResponseModelForV2Api universalResponseModelForV2Api = new UniversalResponseModelForV2Api(null, UniversalResponseModelForV2Api.responseCodes.OK,dataGrablingService.getUpdateStatus(),null,System.currentTimeMillis());
        Gson gson = new Gson();
        return ResponseEntity.ok(gson.toJson(universalResponseModelForV2Api));
    }
    @RequestMapping("/last")
    ResponseEntity<String> last(){
        UniversalResponseModelForV2Api universalResponseModelForV2Api = new UniversalResponseModelForV2Api(null, UniversalResponseModelForV2Api.responseCodes.OK,dataGrablingService.getLastUpdates(),null,System.currentTimeMillis());
        Gson gson = new Gson();
        return ResponseEntity.ok(gson.toJson(universalResponseModelForV2Api));
    }

}
