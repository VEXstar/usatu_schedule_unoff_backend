package ru.vex_core.usatu.controllers.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.vex_core.usatu.services.api.v2.DataStorageServiceV2;

@RestController
@EnableAutoConfiguration
@RequestMapping(value = "/api/v2", produces = "text/plain;charset=UTF-8",method = RequestMethod.GET)
public class UserApiV2Controller {
    @Autowired
    DataStorageServiceV2 dataStorageServiceV2;

    @RequestMapping("/get/schedule/group")
    ResponseEntity<String> getScheduleForGroup(Integer id,Integer sem)
    {
        return dataStorageServiceV2.getRaspByGroupID(id,sem);
    }

    @RequestMapping("/get/schedule/teacher")
    ResponseEntity<String> getScheduleForTeacher(Integer id,Integer sem)
    {
        return dataStorageServiceV2.getRaspByTeacherID(id,sem);
    }
    @RequestMapping("/get/schedule/auditory")
    ResponseEntity<String> getScheduleForAud(Integer id,Integer sem)
    {
        return dataStorageServiceV2.getRaspByAudID(id,sem);
    }
    @RequestMapping("/get/exam/auditory")
    ResponseEntity<String> getExamForAud(Integer id,Integer sem)
    {
        return dataStorageServiceV2.getExamByAudID(id,sem);
    }

    @RequestMapping("/get/exam/group")
    ResponseEntity<String> getExamForGroup(Integer id,Integer sem)
    {
        return dataStorageServiceV2.getExamByGroupID(id,sem);
    }

    @RequestMapping("/get/exam/teacher")
    ResponseEntity<String> getExamForTeacher(Integer id,Integer sem)
    {
        return dataStorageServiceV2.getExamByTeacherID(id,sem);
    }

    @RequestMapping("/find/group")
    ResponseEntity<String> findGroup(Integer id,String term,Integer lvl,Integer fac,Integer student,String stdterm)
    {
        return dataStorageServiceV2.findGroupBy(id,term,lvl,fac,student,stdterm);
    }
    @RequestMapping("/find/teacher")
    ResponseEntity<String> findTeacher(Integer id,String term,Integer dep,Integer fac)
    {
        return dataStorageServiceV2.findTeacherBy(id,term,dep,fac);
    }

    @RequestMapping("/find/auditory")
    ResponseEntity<String> findAud(Integer id,String term,Integer floor,Integer build)
    {
        return dataStorageServiceV2.getAuditories(id,floor,build,term);
    }
    @RequestMapping("/utils/available")
    ResponseEntity<String> check()
    {
        return ResponseEntity.ok("true");
    }

    @RequestMapping("/find/department")
    ResponseEntity<String> findDepartment(Integer fac)
    {
        return dataStorageServiceV2.findDepartmentByFacId(fac);
    }
    @RequestMapping("/get/build")
    ResponseEntity<String> getBuilds()
    {
        return dataStorageServiceV2.getBuilds();
    }
    @RequestMapping("/get/facs")
    ResponseEntity<String> getFacs()
    {
        return dataStorageServiceV2.getFaculties();
    }
    @RequestMapping("/get/sems")
    ResponseEntity<String> getSemesters()
    {
        return dataStorageServiceV2.getSemesters();
    }
    @RequestMapping("/get/week/calc")
    ResponseEntity<String> getWeekNowcalc()
    {
        return dataStorageServiceV2.getWeekByCalc();
    }
    @RequestMapping("/find/student")
    ResponseEntity<String> findStydentBy(String term,Integer group,Integer id)
    {
        return dataStorageServiceV2.findStudentBy(term,group,id);
    }



    //TODO: расписать поиск/получение оставхшихся сущностей, поиск пустых аудиторий на неделю
}
