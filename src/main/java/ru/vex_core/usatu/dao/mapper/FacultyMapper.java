package ru.vex_core.usatu.dao.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;
import ru.vex_core.usatu.models.pub.Faculty;

import java.util.List;
import java.util.Map;

@Service
@Mapper
public interface FacultyMapper {

    List<Faculty> getFacultiesBy(@Param("params") Map<String,Object> params);

    Integer getRows();

    void cleanFacs();

    void addAllFacs(@Param("list") List<Faculty> list);
}
