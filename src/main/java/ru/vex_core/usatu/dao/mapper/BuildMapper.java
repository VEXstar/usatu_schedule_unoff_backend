package ru.vex_core.usatu.dao.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;
import ru.vex_core.usatu.models.pub.Build;

import java.util.List;
import java.util.Map;

@Service
@Mapper
public interface BuildMapper {


    List<Build> getBuildsBy(@Param("params") Map<String,Object> params);

    void removeBuildsBy (@Param("params") Map<String,Object> params);

    void addAllBuilds(@Param("list") List<Build> list);

    Integer getRows();
}
