package ru.vex_core.usatu.dao.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Mapper
public interface LastUpdateMapper {

    void updateTimeStamp(@Param("timestamp") Long timestamp,@Param("mode") String mode);

    List<Long> getLastUpdate(@Param("mode") String mode);
}
