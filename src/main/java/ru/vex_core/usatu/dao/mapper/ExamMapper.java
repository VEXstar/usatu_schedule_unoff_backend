package ru.vex_core.usatu.dao.mapper;


import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;
import ru.vex_core.usatu.models.pub.Exam;

import java.util.List;
import java.util.Map;

@Service
@Mapper
public interface ExamMapper {

    List<Exam> getExamsBy( @Param("params") Map<String,Object> params);

    void addAllExams(@Param("list") List<Exam> list);

    void removeExamsBy(@Param("list")List<Integer> list);


    Integer getRows();
}
