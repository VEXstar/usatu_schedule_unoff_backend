package ru.vex_core.usatu.dao.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;
import ru.vex_core.usatu.models.pub.Auditory;

import java.util.List;
import java.util.Map;

@Service
@Mapper
public interface AuditoryMapper {

    List<Auditory> getAuditoriesBy(@Param("params") Map<String,Object> params);

    void removeAuditoriesBy (@Param("params") Map<String,Object> params);

    void addAllAuditories(@Param("list") List<Auditory> list);

    Integer getRows();




}
