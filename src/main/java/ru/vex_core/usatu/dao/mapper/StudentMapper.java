package ru.vex_core.usatu.dao.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;
import ru.vex_core.usatu.models.pub.Student;

import java.util.List;
import java.util.Map;

@Service
@Mapper
public interface StudentMapper {

    List<Student> getStudentsBy(@Param("params")Map<String,Object> params);

    List<Student> getStudentsByTerm(@Param("param") String param);

    void removeStudentsBy(@Param("params")Map<String,Object> params);

    Integer getRows();

    void addAllStudents(@Param("list") List<Student> list);

}
