package ru.vex_core.usatu.dao.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;
import ru.vex_core.usatu.models.pub.Semester;

import java.util.List;

@Service
@Mapper
public interface SemesterMapper {

    List<Semester> getAllSemesters();

    Integer getRows();

    void cleanSemesters();

    void addAll(@Param("list") List<Semester> list);

}
