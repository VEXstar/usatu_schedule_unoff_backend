package ru.vex_core.usatu.dao.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;
import ru.vex_core.usatu.models.pub.Schedule;

import java.util.List;
import java.util.Map;

@Service
@Mapper
public interface ScheduleMapper {

    List<Schedule> getSchedulesBy(@Param("params") Map<String,Object> params);

    void addAllSchedules(@Param("list") List<Schedule> list);

    void removeSchedulesByList(@Param("list")List<Integer> list);

    void removeSchedulesBy(@Param("params") Map<String,Object> params);


    Integer getRows();
}
