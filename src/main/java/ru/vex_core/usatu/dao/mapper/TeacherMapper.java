package ru.vex_core.usatu.dao.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;
import ru.vex_core.usatu.models.pub.Teacher;

import java.util.List;
import java.util.Map;

@Service
@Mapper
public interface TeacherMapper {

    List<Teacher> getTeachersBy(@Param("params")Map<String,Object> params);

    List<Teacher> getTeachersByTerm(@Param("param") String param);

    Integer getRows();

    void addAllTeacher(@Param("list") List<Teacher> list);

    void removeTeachersBy(@Param("params")Map<String,Object> params);
}
