package ru.vex_core.usatu.dao.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;
import ru.vex_core.usatu.models.pub.Group;

import java.util.List;
import java.util.Map;

@Service
@Mapper
public interface GroupMapper {
    List<Group> getGroupsBy(@Param("params") Map<String,Object> params);
    List<Group> getgetGroupsByStdTerm(@Param("params") Map<String,Object> params);

    List<Group> getGroupsByTerm(@Param("param") String param);

    void addAllGroups(@Param("list") List<Group> list);

    Integer getRows();

    void removeGroupsBy(@Param("params") Map<String,Object> params);
}
