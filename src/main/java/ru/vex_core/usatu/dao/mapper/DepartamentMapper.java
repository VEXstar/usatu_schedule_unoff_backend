package ru.vex_core.usatu.dao.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;
import ru.vex_core.usatu.models.pub.Department;

import java.util.List;
import java.util.Map;

@Service
@Mapper
public interface DepartamentMapper {

    List<Department> getDepartamentsBy(@Param("params") Map<String,Object> params);

    void addAllDepartaments(@Param("list") List<Department> list);

    void cleanDepartaments();

    Integer getRows();


}
