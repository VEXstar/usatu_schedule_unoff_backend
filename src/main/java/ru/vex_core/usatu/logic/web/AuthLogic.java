package ru.vex_core.usatu.logic.web;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.*;

public class AuthLogic {
    private String username;
    private String password;
    private String tokenHTML;
    private String tokenCookie;
    private String sessionID;
    private String host;
    private String urlHost;
    private String classCaller;


    public AuthLogic(String username, String password, String hostUri,String className){
        this.username = username;
        this.classCaller = className;
        this.password = password;
        this.host = hostUri;
        this.urlHost = "https://"+hostUri+"/";
        reLogin();

    }



    private void resetTokens(){
        ResponseEntity<String> responseEntity = HttpClient.doGet(urlHost, null, null);
        sessionID = getSessionIDinCookie(responseEntity);
        tokenCookie = getTokenInCookie(responseEntity);
        tokenHTML = getTokenInHtml(responseEntity);
    }
    private void updateTokens(){
        MultiValueMap<String,String> head = new LinkedMultiValueMap<>();
        head.add("Accept","*/*");
        head.add("Cookie","sessionid="+sessionID+"; csrftoken="+sessionID);
        head.add("Cache-Control","no-cache");
        head.add("Connection","keep-alive");
        head.add("Referer",urlHost);
        ResponseEntity<String> responseEntity = HttpClient.doGet(urlHost+"raspisanie/", head, null);
        tokenCookie = getTokenInCookie(responseEntity);
        tokenHTML = getTokenInHtml(responseEntity);
    }

    private void loginUP(){

        MultiValueMap<String,String> body = new LinkedMultiValueMap<>();
        body.add("csrfmiddlewaretoken",tokenHTML);
        body.add("user_login",username);
        body.add("user_password",password);

        MultiValueMap<String,String> head = new LinkedMultiValueMap<>();

        head.add("Accept","*/*");
        head.add("Cookie","csrftoken="+tokenCookie);
        head.add("Accept-Encoding","gzip, deflate, br");
        head.add("Cache-Control","no-cache");
        head.add("Connection","keep-alive");
        head.add("Referer",urlHost);

        ResponseEntity<String> responseEntity = HttpClient.doPost(urlHost, MediaType.APPLICATION_FORM_URLENCODED, head, body);

        if(responseEntity.getStatusCode()== HttpStatus.FOUND){
            tokenCookie = getTokenInCookie(responseEntity);
            sessionID = getSessionIDinCookie(responseEntity);
            updateTokens();
            System.out.println("Sucsesful auth for "+classCaller);
            return;
        }
        System.out.println("Auth Faild with http error code: "+responseEntity.getStatusCode());


    }


    private String getSessionIDinCookie(ResponseEntity<String> responseEntity)
    {
        Map<String,List<String>> headers = responseEntity.getHeaders();
        for (String value: Objects.requireNonNull(headers.get("Set-Cookie")))
        {
            String data = Arrays.stream(value.split(";")).filter(s->s.startsWith("sessionid=")).findFirst().orElse(null);
            if(data!=null)
                return data.replaceAll("sessionid=","");
        }
        return null;
    }


    private String getTokenInHtml(ResponseEntity<String> responseEntity)  {

        Document document = Jsoup.parse(Objects.requireNonNull(responseEntity.getBody()));
        return document.select("form>input").get(0).attr("value");

    }
    private String getTokenInCookie(ResponseEntity<String> responseEntity)
    {
        Map<String, List<String>> headers = responseEntity.getHeaders();
        for (String value: Objects.requireNonNull(headers.get("Set-Cookie")))
        {
            String data = Arrays.stream(value.split(";")).filter(s->s.startsWith("csrftoken=")).findFirst().orElse(null);
            if(data!=null)
                return data.replaceAll("csrftoken=","");
        }
        return null;

    }
    Integer tokenDie = 72;
    public void subscribeTokensFor(MultiValueMap<String,String> headers,MultiValueMap<String,String> body){
            body.add("csrfmiddlewaretoken",tokenHTML);
            headers.remove("Cookie");
            headers.remove("Referer");
            headers.add("Cookie","sessionid="+sessionID+"; csrftoken="+sessionID);
            headers.add("Referer",urlHost);
            tokenDie--;
            if(tokenDie<=0){
                tokenDie = 72;
                updateTokens();
            }
    }
    public void reLogin(){
        resetTokens();
        loginUP();
    }
}
