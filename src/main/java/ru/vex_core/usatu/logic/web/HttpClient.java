package ru.vex_core.usatu.logic.web;

import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.TrustStrategy;
import org.springframework.context.annotation.Bean;
import org.springframework.http.*;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.RestTemplate;

import javax.net.ssl.SSLContext;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.concurrent.CountDownLatch;

public class HttpClient {

    public static ResponseEntity<String> doGet(String url,MultiValueMap<String,String> headers,MultiValueMap<String,String> params){
        if(headers==null)
            headers = new LinkedMultiValueMap<>();
        if(params==null)
            params = new LinkedMultiValueMap<>();
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Content-Type", "text/html; charset=utf-8");
        httpHeaders.addAll(headers);
        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(httpHeaders);
        return  (getRestTemplateBypassingHostNameVerifcation()).exchange(url, HttpMethod.GET,request,String.class,params);
    }

    public static ResponseEntity<String> doPost(String url,MediaType mediaType, MultiValueMap<String,String> headers,MultiValueMap<String,String> body){

        if(headers==null)
            headers = new LinkedMultiValueMap<>();
        if(body==null)
            body = new LinkedMultiValueMap<>();


        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.addAll(headers);
        httpHeaders.add("Content-Type", "text/html; charset=utf-8");
        httpHeaders.setContentType(mediaType);
        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(body, httpHeaders);
        return  (getRestTemplateBypassingHostNameVerifcation()).postForEntity(url, request , String.class);

    }

    @Bean
    public static RestTemplate getRestTemplateBypassingHostNameVerifcation() {
        TrustStrategy acceptingTrustStrategy = (X509Certificate[] chain, String authType) -> true;

        SSLContext sslContext = null;
        try {
            sslContext = org.apache.http.ssl.SSLContexts.custom()
                    .loadTrustMaterial(null, acceptingTrustStrategy)
                    .build();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        }

        SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext);

        CloseableHttpClient httpClient = HttpClients.custom()
                .setSSLSocketFactory(csf)
                .build();

        HttpComponentsClientHttpRequestFactory requestFactory =
                new HttpComponentsClientHttpRequestFactory();

        requestFactory.setHttpClient(httpClient);
        RestTemplate restTemplate = new RestTemplate(requestFactory);
        return restTemplate;
    }

    public static ResponseEntity<String> doPost(String url, MultiValueMap<String,String> headers,MultiValueMap<String,String> body) throws InterruptedException {

        if(headers==null)
            headers = new LinkedMultiValueMap<>();
        if(body==null)
            body = new LinkedMultiValueMap<>();


        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.addAll(headers);
        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(body, httpHeaders);
        RestTemplate restTemplate = getRestTemplateBypassingHostNameVerifcation();
        restTemplate.setErrorHandler(new ResponseErrorHandler() {
            @Override
            public boolean hasError(ClientHttpResponse clientHttpResponse) throws IOException {
                return false;
            }

            @Override
            public void handleError(ClientHttpResponse clientHttpResponse) throws IOException {

            }
        });
        final ResponseEntity<String>[] responseEntity = new ResponseEntity[]{null};
        final CountDownLatch latch = new CountDownLatch(1);
        MultiValueMap<String, String> finalBody = body;
        Thread thread = new Thread(() -> {
            int tryes = 5;
            while (tryes>0&&responseEntity[0]==null){
                tryes--;
                try {
                    ResponseEntity<String> entity = (restTemplate).postForEntity(url, request, String.class);
                    responseEntity[0] = entity;
                    break;
                }catch (Exception e){
                    System.out.println("[ERROR][!!!][HTTPCLIENT] requset faild with error "+e.getMessage() +"\n body request: " + finalBody.toString());
                }
                try { Thread.sleep((5-tryes)*2000); } catch (InterruptedException ignored) {}
            }
            latch.countDown();

        });
        thread.start();
        latch.await();
        return responseEntity[0];

    }

}
