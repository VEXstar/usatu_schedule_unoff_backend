package ru.vex_core.usatu.logic.parse.data;

import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import ru.vex_core.usatu.providers.WebRequsetUSATUProvider;

import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DataParser {
    protected WebRequsetUSATUProvider web;
    protected String workUri;

    protected DataParser(WebRequsetUSATUProvider web, String workUri) {
        this.web = web;
        this.workUri = workUri;
    }

    protected String requester(MultiValueMap<String,String> body,String urlPost) throws InterruptedException {
        MultiValueMap<String,String> headers = new LinkedMultiValueMap<>();
        headers.add("X-Requested-With","XMLHttpRequest");
        Set<String> hexItems = new HashSet<>();
        String resp = web.getData("post",urlPost,headers,body);
        Matcher m = Pattern.compile("\\\\u[a-fA-f0-9]{4}").matcher(resp);
        while (m.find()) {
            hexItems.add(m.group());
        }
        for (String unicodeHex : hexItems) {
            int hexVal = Integer.parseInt(unicodeHex.substring(2), 16);
            resp = resp.replace(unicodeHex, "" + ((char) hexVal));
        }
        return resp;

    }
}
