package ru.vex_core.usatu.logic.parse.data;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import ru.vex_core.usatu.models.pub.Auditory;
import ru.vex_core.usatu.models.pub.Build;
import ru.vex_core.usatu.models.pub.Semester;
import ru.vex_core.usatu.models.usatu.UsatuResponceData;
import ru.vex_core.usatu.providers.WebRequsetUSATUProvider;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class BuildingParser extends DataParser {
    List<Build> builds;
    List<Auditory> auditories;
    List<Semester> semesters;



    public BuildingParser(WebRequsetUSATUProvider webProvider, String url) throws InterruptedException {
        super(webProvider,url);

        builds = new ArrayList<>();
        auditories = new ArrayList<>();
        semesters = new ArrayList<>();
        Document get = Jsoup.parse(webProvider.getData("get", url, null, null));
        parseSems(get);
        parseBuilds(get);
        parseAuds();
        parseAndMergeGhostAuds();
    }

    public List<Semester> getSemesters() {
        return semesters;
    }

    private void parseSems(Document doc){
        Elements sems = doc.select("#SemestrSchedule").get(0).select("option");
        AtomicInteger i = new AtomicInteger(1);
        sems.forEach(x->{
            if(!x.val().equals("")) {
                semesters.add(new Semester(Integer.parseInt(x.val()),x.text()));
                System.out.println("[INFO][BUILDPARSE] Semester Parsed: "+x.text());
            }
        });
    }

    private void parseBuilds(Document doc){
        Elements builds = doc.select("#id_building").get(0).select("option");
        AtomicInteger i = new AtomicInteger(1);
        builds.forEach(x->{
            if(!x.val().equals("")) {
                this.builds.add(new Build(i.getAndIncrement(),x.val(),x.text()));
                System.out.println("[INFO][BUILDPARSE] Build Parsed: "+x.text());
            }
        });
    }

    private void parseAndMergeGhostAuds(){
        MultiValueMap<String,String> body = new LinkedMultiValueMap<>();
        body.add("floor","");
        body.add("building","");
        List<UsatuResponceData> usatuResponceData = parseAudWithBody(body);
        List<Auditory> ghost = new ArrayList<>();
        usatuResponceData.forEach(allOne->{
            boolean isnew = true;
            Integer allOneId = Integer.parseInt(allOne.getId());
            for (Auditory auditory : auditories) {

                if(allOne.getMane().equals(auditory.getName())&&allOneId.equals(auditory.getId())){
                    isnew = false;
                    break;
                }
            }
            if(isnew)
                ghost.add(new Auditory(allOneId,allOne.getMane(),0,0));
        });
        auditories.addAll(ghost);

    }

    private void parseAuds(){
        MultiValueMap<String,String> body = new LinkedMultiValueMap<>();
        for (int i = 1;i<=6;i++){
            body.remove("floor");
            body.add("floor",i+"");
            int finalI = i;
            builds.forEach(v->{
                body.remove("building");
                body.add("building",v.getTitle());
                List<UsatuResponceData> usatuResponceData = parseAudWithBody(body);
                usatuResponceData.forEach(k->{
                    auditories.add(new Auditory(Integer.parseInt(k.getId()),k.getMane(),finalI,v.getId()));
                    System.out.println("[INFO][BUILDPARSE]["+v.getTitle()+"] Parsed auditory: "+k.getMane());
                });
            });
        }
    }

    private List<UsatuResponceData> parseAudWithBody( MultiValueMap<String,String> body){
        String groupsInJson = null;
        try {
            groupsInJson = requester(body,workUri);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Gson gson = new Gson();
        return gson.fromJson(groupsInJson,new TypeToken< List<UsatuResponceData>>(){}.getType());

    }

    public List<Build> getBuilds() {
        return builds;
    }

    public List<Auditory> getAuditories() {
        return auditories;
    }
}
