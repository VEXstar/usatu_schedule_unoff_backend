package ru.vex_core.usatu.logic.parse.scheulde;

import org.apache.commons.lang3.ArrayUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import ru.vex_core.usatu.models.pub.Debug;
import ru.vex_core.usatu.models.pub.Group;
import ru.vex_core.usatu.models.pub.Schedule;
import ru.vex_core.usatu.models.pub.Semester;
import ru.vex_core.usatu.providers.WebRequsetUSATUProvider;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ScheuldeParser extends Parser {

    public List<Schedule> getSchedules() {
        return schedules;
    }

    private List<Schedule> schedules;
    private List<Schedule>[][][] schedulesCube;


    public ScheuldeParser(WebRequsetUSATUProvider web, String workUri, List<Group> group, List<Semester> semester, Debug debug) {
        super(web, workUri, group, semester);

        fillCube(semester.size());
        schedules = Collections.synchronizedList(new ArrayList());




        parseRasp(group,semester,debug);
    }

    public ScheuldeParser(WebRequsetUSATUProvider web, String workUri) {
        super(web, workUri, null, null);
        schedules = Collections.synchronizedList(new ArrayList());


    }

    private void  fillCube(Integer sems){
        schedulesCube = new List[sems][6][7];
        for (int s = 0; s<sems;s++){
            for(int d = 0; d<6;d++){
                for(int n = 0; n<7;n++){
                    schedulesCube[s][d][n] = Collections.synchronizedList(new ArrayList());
                }
            }
        }
    }

    @Override
    public void debugParse(Integer grID, int sem) {
        List<Group> gl = new ArrayList<>();
        gl.add(new Group("debug group",-1,grID,-1,-1));
        List<Semester> sl = new ArrayList<>();
        sl.add(new Semester(sem,"Весенний семестр 2018-2019"));
        parseRasp(gl,sl,null);
    }

    private void parseRasp(List<Group> group, List<Semester> semester,Debug debug){

        AtomicInteger atomicInteger = new AtomicInteger(1);
        Integer c = group.size()*semester.size();

        semester.forEach(sem->{
            CountDownLatch countDownLatch = new CountDownLatch(4);
            int rz = 4;
            for(int i =0; i <group.size();i++){
                rz--;
                Group grp = group.get(i);
                CountDownLatch finalCountDownLatch = countDownLatch;
                Thread thread = new Thread(()->{
                    MultiValueMap<String,String> body = new LinkedMultiValueMap<>();
                    body.add("ScheduleType","За семестр");
                    body.add("view","ПОКАЗАТЬ");
                    body.add("group",grp.getId()+"");
                    body.add("sem",sem.getId()+"");
                    String data = null;
                    try {
                        MultiValueMap<String,String> headers = new LinkedMultiValueMap<>();
                        headers.add("Content-Type","application/x-www-form-urlencoded");
                        data = web.getData("post",workUri,headers,body);
                        Document parse = Jsoup.parse(data);
                        parseOneTableOfRasp(parse,grp.getTitle(),sem.getTitle(),grp.getId(),atomicInteger.getAndIncrement(),c,sem.getId(),semester.indexOf(sem));
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    debug.setStatus("Update schedule at "+atomicInteger.get()+"/" +c);
                    finalCountDownLatch.countDown();
                });
                thread.start();
                if(rz==0){
                    rz = 4;
                    try { countDownLatch.await(); } catch (InterruptedException e) { }
                    countDownLatch = new CountDownLatch(rz);
                }

            }
        });
        mergeDublicates(semester.size());
    }
    private void parseOneTableOfRasp(Document doc, String grp, String sem, Integer grID, Integer numof,Integer count,Integer semID,Integer semIndex){
        if(doc.select("#schedule").size()==0){
            System.out.println("[WARN][SCHEULDEPARSE] Fail parse scheulde for group: "+grp+" in sem: "+sem+" "+numof+"/"+count);
            return;
        }
        Elements lines = doc.select("#schedule").get(0).getElementsByTag("table").get(0).getElementsByTag("tbody").get(0).getElementsByTag("tr");
        lines.forEach(tr->{
            Elements tds = tr.children();
            int i = 0;
            int numNow = 1;
            for (Element td: tds){
                if(td.getElementsByClass("font-couple").size()>0){
                    numNow = Integer.parseInt(td.getElementsByClass("font-couple").get(0).text().replaceAll(" пара",""));
                    continue;
                }
                i++;
                if(td.text().isEmpty()){
                    continue;
                }
                List<Schedule> parsed = genSchedules(td.html(),grID,i,numNow,semID);
                parsed.forEach(v->{
                    schedulesCube[semIndex][v.getDayOfWeek()-1][v.getNumOfDay()-1].add(v);
                });
            }
        });
        System.out.println("[INFO][SCHEULDEPARSE] Sucsesful parse schedule for group: "+grp+" in sem: "+sem+" "+numof+"/"+count);
    }
    private List<Schedule> genSchedules(String code,Integer gID,int day,int num,Integer semID){
        List<Schedule> ss = new ArrayList<>();
        Pattern patternWithTeacher = Pattern.compile("<br><p><font class=\"font-subject\">(.[^A-Z]*?)</font><br>" +
                "<font class=\"font-classroom\"><a href=\"#\" onclick=\"return false\" onmouseup=\"GoToTheLink(.[^A-Z]*?)\">(.[^A-Z]*?)</a></font>" +
                "<font class=\"font-teacher\"></font></p><p><font class=\"font-teacher\">(.[^A-Z]*?)</font></p>" +
                "<font class=\"font-teacher\"><a href=\"#\" onclick=\"return false\" onmouseup=\"GoToTheLink(.[^A-Z]*?)\">(.[^A-Z]*?)</a><br></font><p>(.[- ,0-9]*?)</p>");


        Pattern patternWithOutTEacher = Pattern.compile("<br><p><font class=\"font-subject\">(.[^A-Z]*?)</font><br>" +
                "<font class=\"font-classroom\"><a href=\"#\" onclick=\"return false\" onmouseup=\"GoToTheLink(.[^A-Z]*?)\">(.[^A-Z]*?)</a></font>" +
                "<font class=\"font-teacher\"></font></p><p><font class=\"font-teacher\">(.[^A-Z]*?)</font></p><p>(.[- ,0-9]*?)</p>");
        code = code.replaceAll("\n","");
        Matcher matcherT = patternWithTeacher.matcher(code);
        Matcher matcherNT = patternWithOutTEacher.matcher(code);

        Integer[] g = new Integer[]{gID};
        while (matcherT.find()){
            ss.add(new Schedule(matcherT.group(1),-1,parseWeeks(matcherT.group(7)),getID(matcherT.group(5)),g, getID(matcherT.group(2)),getType(matcherT.group(4)),day,num,semID));
        }
        while (matcherNT.find()){
            Integer l = new Integer(-1);
            ss.add(new Schedule(matcherNT.group(1),-1,parseWeeks(matcherNT.group(5)),l,g,getID(matcherNT.group(2)),getType(matcherNT.group(4)),day,num,semID));
        }
        return ss;

    }
    private void mergeDublicates(Integer sems){
        System.out.println("[INFO][SCHEULDEPARSE] Merge List start\n Total elems: "+ schedules.size() );
        CountDownLatch countDownLatch = new CountDownLatch(sems*6*7);
        for (int s = 0; s<sems;s++){
            for(int d = 0; d<6;d++){
                for(int n = 0; n<7;n++){
                    int finalS = s;
                    int finalD = d;
                    int finalN = n;
                    Thread thread = new Thread(()->{

                        List<Schedule> workZone = schedulesCube[finalS][finalD][finalN];

                        Integer i =  new Integer(0);
                        while (i<workZone.size()){
                            for (i=0; i<workZone.size();i++){
                                Schedule one = workZone.get(i);
                                Schedule find =  workZone.stream().filter(olo->olo!=one&&one.getLessonName().equals(olo.getLessonName())&&
                                        one.getTeachersIDs().equals(olo.getTeachersIDs())&&
                                        one.getAuditoryID().equals(olo.getAuditoryID()) &&
                                        Arrays.equals(one.getWeeks(), olo.getWeeks())).findFirst().orElse(null);
                                if(find!=null){
                                    one.setGroupsIds(ArrayUtils.addAll(one.getGroupsIds(),find.getGroupsIds()));
                                    workZone.remove(find);
                                    break;
                                }
                            }
                        }
                        schedules.addAll(workZone);
                        System.out.println("[INFO][SCHEULDEPARSE] Merged cube ["+finalS+"]["+finalD+"]["+finalN+"]");
                        countDownLatch.countDown();
                    });
                    thread.start();
                }
            }
        }
        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("[INFO][SCHEULDEPARSE] Merge List end");
    }





    private Schedule.typeOfLesson getType(String st){
        st = st.toLowerCase();
        if(st.equals("лекция"))
            return Schedule.typeOfLesson.LECTION;
        if(st.equals("практика (семинар)"))
            return Schedule.typeOfLesson.PRACTIC;
        if(st.equals("лекция + практика"))
            return Schedule.typeOfLesson.LESSONandPRACTIC;
        if(st.equals("лабораторная работа"))
            return Schedule.typeOfLesson.LAB;
        if(st.equals("лекция + практика + лабораторная работа"))
            return Schedule.typeOfLesson.LPL;
        if(st.equals("консультация"))
            return Schedule.typeOfLesson.CONSULTATION;
        if(st.equals("физвоспитание"))
            return Schedule.typeOfLesson.PHYEDUCATION;
        if(st.equals("военная подготовка"))
            return Schedule.typeOfLesson.ARMYBOOTS;
        if(st.equals("прочее"))
            return Schedule.typeOfLesson.OTHER;
        return Schedule.typeOfLesson.OTHER;
    }

    private  Integer[] parseWeeks(String week)
    {
        String[] splited = week.replaceAll(" ","").split(",");
        Integer[] to_resp = new Integer[31];
        Arrays.fill(to_resp,0);
        for (String aSplited : splited) {
            if (aSplited.contains("-")) {
                int first = Integer.parseInt(aSplited.split("-")[0]);
                int last = Integer.parseInt(aSplited.split("-")[1]);
                if(last>=to_resp.length)
                    last = to_resp.length-1;
                for (int j = first; j <= last; j++)
                    to_resp[j] = 1;
            } else
            {
                int pre =Integer.parseInt(aSplited);
                if(pre>=to_resp.length)
                    pre = to_resp.length-1;
                to_resp[pre] = 1;
            }
        }
        return to_resp;
    }


}
