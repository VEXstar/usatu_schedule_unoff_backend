package ru.vex_core.usatu.logic.parse.data;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import ru.vex_core.usatu.models.pub.*;
import ru.vex_core.usatu.models.usatu.UatuResponseStudentData;
import ru.vex_core.usatu.models.usatu.UsatuResponceData;
import ru.vex_core.usatu.models.pub.*;
import ru.vex_core.usatu.providers.WebRequsetUSATUProvider;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class HumanParser extends DataParser {

    private List<Faculty> facs;
    private List<Group> groups;
    private List<Student> students;
    private List<Department> departments;
    private List<Teacher> teachers;
    private String url2;





    public HumanParser(WebRequsetUSATUProvider webProvider, String url,String urlGhost) throws InterruptedException {
        super(webProvider,url);
        url2 = urlGhost;
        groups = new ArrayList<>();
        students = new ArrayList<>();
        departments = new ArrayList<>();
        teachers = new ArrayList<>();
        facs = new ArrayList<>();

        String data = webProvider.getData("get",url,null,null);
        parseFacs(Jsoup.parse(data));
        parseDepAndTeachers();
        parseAndMergeGhostTeachers();
        parseGroupsAndStudents();



    }

    private void parseFacs(Document doc){
        Elements facsInHtml = doc.select("#id_faculty").get(0).select("option");

        AtomicInteger i = new AtomicInteger(1);
        facsInHtml.forEach(x->{
            if(!x.val().equals("")) {
                facs.add(new Faculty(x.text(), i.getAndIncrement(),x.val()));
                System.out.println("[INFO][HUMANPARSE] Faculty Parsed: "+x.text());
            }
        });
    }

    private void parseGroupsAndStudents(){
        MultiValueMap<String,String> body = new LinkedMultiValueMap<>();

        body.add("MessageType","студенту");
        for (int i = 1;i<=6;i++){
            body.remove("klass");
            body.add("klass",i+"");
            int finalI = i;
            facs.forEach(v->{
                body.remove("faculty");
                body.add("faculty",v.getReqName());

                String groupsInJson = null;
                try {
                    groupsInJson = requester(body,workUri);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Gson gson = new Gson();
                System.out.println("[INFO][HUMANPARSE] Start parse groups for fac: "+v.getTitle()+" klass: "+finalI);
                List<UsatuResponceData> responceData = gson.fromJson(groupsInJson,new TypeToken< List<UsatuResponceData>>(){}.getType());

                responceData.forEach(k->{
                    int stdCount = 0;
                    try {
                        stdCount = parseStudents(Integer.parseInt(k.getId()));
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    groups.add(new Group(k.getMane(), finalI,Integer.parseInt(k.getId()),stdCount,v.getId()));
                    System.out.println("[INFO][HUMANPARSE]["+v.getTitle()+"] Parsed group: "+k.getMane());
                });
            });

        }

    }

    private int parseStudents(int groupID) throws InterruptedException {
        MultiValueMap<String,String> body = new LinkedMultiValueMap<>();
        body.add("MessageType","студенту");
        body.add("GroupOrUser","1");
        body.add("group",groupID+"");
        Gson gson = new Gson();
        List<UatuResponseStudentData> responseData = gson.fromJson(requester(body,workUri),new TypeToken< List<UatuResponseStudentData>>(){}.getType());
        responseData.forEach(k->{
            String[] delim = k.getName().split(" ");
            students.add(new Student(delim[1],delim[0],delim[2],Integer.parseInt(k.getId()),groupID));
        });
        return responseData.size();
    }

    private void parseAndMergeGhostTeachers() throws InterruptedException {
        MultiValueMap<String,String> body = new LinkedMultiValueMap<>();
        body.add("kafedra","");
        List<UatuResponseStudentData> usatuResponceData = parseTeachersWithBody(body,url2);
        List<Teacher> ghost = new ArrayList<>();
        usatuResponceData.forEach(allOne->{
            Integer allOneId = Integer.parseInt(allOne.getId());
            boolean isnew = true;
            for (Teacher teacher : teachers) {
                if(allOne.getName().equals(teacher.getTeacherName())&& allOneId.equals(teacher.getId())){
                    isnew = false;
                    break;
                }
            }
            if(isnew)
                ghost.add(new Teacher(allOne.getName(),0,0,allOneId,0));
        });
        teachers.addAll(ghost);

    }

    private void parseDepAndTeachers(){
        MultiValueMap<String,String> body = new LinkedMultiValueMap<>();
        body.add("MessageType","преподавателю");
        facs.forEach(v->{
            body.remove("faculty");
            body.add("faculty",v.getReqName());
            Gson gson = new Gson();
            System.out.println("[INFO][HUMANPARSE] Start parse departments for fac: "+v.getTitle());
            List<UsatuResponceData> responceData = null;
            try {
                responceData = gson.fromJson(requester(body,workUri),new TypeToken<List<UsatuResponceData>>(){}.getType());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            responceData.forEach(k->{

                    departments.add(new Department(k.getMane(),Integer.parseInt(k.getId()),v.getId()));
                try {
                    parseTeachers(Integer.parseInt(k.getId()),v.getId());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("[INFO][HUMANPARSE]["+v.getTitle()+"] Parsed department: "+k.getMane());

            });
        });
    }

    private void parseTeachers(Integer depID,Integer facID) throws InterruptedException {
        MultiValueMap<String,String> body = new LinkedMultiValueMap<>();
        body.add("MessageType","преподавателю");
        body.add("GroupOrUser","1");
        body.add("dep",depID+"");
        List<UatuResponseStudentData> responseData = parseTeachersWithBody(body,workUri);
        responseData.forEach(k->{
            teachers.add(new Teacher(k.getName(),depID,facID,Integer.parseInt(k.getId()),-1));
        });
    }

    private List<UatuResponseStudentData> parseTeachersWithBody( MultiValueMap<String,String> body,String postUrl) throws InterruptedException {
        Gson gson = new Gson();
        return gson.fromJson(requester(body,postUrl),new TypeToken< List<UatuResponseStudentData>>(){}.getType());
    }



    public List<Faculty> getFacs() {
        return facs;
    }

    public List<Group> getGroups() {
        return groups;
    }

    public List<Student> getStudents() {
        return students;
    }

    public List<Department> getDepartments() {
        return departments;
    }

    public List<Teacher> getTeachers() {
        return teachers;
    }
}
