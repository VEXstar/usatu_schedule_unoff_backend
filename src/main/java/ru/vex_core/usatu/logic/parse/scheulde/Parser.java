package ru.vex_core.usatu.logic.parse.scheulde;

import org.springframework.util.MultiValueMap;
import ru.vex_core.usatu.models.pub.Group;
import ru.vex_core.usatu.models.pub.Semester;
import ru.vex_core.usatu.providers.WebRequsetUSATUProvider;
import java.util.List;

public abstract class Parser {
    protected WebRequsetUSATUProvider web;
    protected String workUri;

    public Parser(WebRequsetUSATUProvider web, String workUri, List<Group> group, List<Semester> semester) {
        this.web = web;
        this.workUri = workUri;
    }

    protected String requester(MultiValueMap<String,String> body) throws InterruptedException {
        return web.getData("post",workUri,null,body);

    }
    protected int getID(String in){
        String[] pre = in.replaceAll("\\)","").split(", ");
        return Integer.parseInt(pre[pre.length-1]);
    }
    public abstract void  debugParse(Integer grID,int sem) throws InterruptedException;
}
