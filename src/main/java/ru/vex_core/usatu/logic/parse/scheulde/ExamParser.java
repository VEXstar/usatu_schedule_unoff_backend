package ru.vex_core.usatu.logic.parse.scheulde;

import org.apache.commons.lang3.ArrayUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import ru.vex_core.usatu.models.pub.Debug;
import ru.vex_core.usatu.models.pub.Exam;
import ru.vex_core.usatu.models.pub.Group;
import ru.vex_core.usatu.models.pub.Semester;
import ru.vex_core.usatu.providers.WebRequsetUSATUProvider;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ExamParser extends Parser {

    private List<Exam> exams;

    public List<Exam> getExams() {
        return exams;
    }

    public ExamParser(WebRequsetUSATUProvider web, String workUri, List<Group> group, List<Semester> semester, Debug debug){
        super(web, workUri, group, semester);
        exams = Collections.synchronizedList(new ArrayList());
        parseExams(group,semester,debug);
    }
    public ExamParser(WebRequsetUSATUProvider web, String workUri) {
        super(web, workUri, null, null);
        exams = Collections.synchronizedList(new ArrayList());

    }

    private void parseExams(List<Group> group, List<Semester> semester,Debug debug) {
        Integer c = group.size()*semester.size();
        AtomicInteger atomicInteger = new AtomicInteger(1);
        group.forEach(grp->{
            CountDownLatch countDownLatch = new CountDownLatch(semester.size());
            semester.forEach(sem->{
                Thread thread = new Thread(()->{
                    MultiValueMap<String,String> body = new LinkedMultiValueMap<>();
                    body.add("ScheduleType","Экзамены");
                    body.add("view","ПОКАЗАТЬ");
                    body.add("group",grp.getId()+"");
                    body.add("sem",sem.getId()+"");
                    try {
                        MultiValueMap<String,String> headers = new LinkedMultiValueMap<>();
                        headers.add("Content-Type","application/x-www-form-urlencoded");
                        parseOneExam(Jsoup.parse(web.getData("post",workUri,headers,body)),grp.getTitle(),sem.getTitle(),grp.getId(),atomicInteger.getAndIncrement(),c,sem.getId());
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    debug.setStatus("Update exam at "+atomicInteger.get()+"/" +c);
                    countDownLatch.countDown();
                });
                thread.start();
            });
            try {
                countDownLatch.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        mergeDublicates();
    }


    private void mergeDublicates(){
        System.out.println("[INFO][EXAMPARSE] Merge List start");
        boolean debugKiller = false;
        Integer i =  new Integer(0);
         while ((i)<exams.size()){
           for (i=0; i<exams.size();i++){
               Exam one = exams.get(i);
               Exam find =  exams.stream().filter(two->one!=two&&one.getTitle().equals(two.getTitle())&& one.getDate().equals(two.getDate())&& one.getSemID().equals(two.getSemID()) &&one.getTypeOfExam()==two.getTypeOfExam()).findFirst().orElse(null);
               if(find!=null){
                   one.setGroupsID(ArrayUtils.addAll(one.getGroupsID(),find.getGroupsID()));
                   exams.remove(find);
                   break;
               }
               if(debugKiller)
                   return;
           }
         }
        System.out.println("[INFO][EXAMPARSE] Merge List end");

    }

    public void  debugParse(Integer grID,int sem) {
        List<Group> gl = new ArrayList<>();
        gl.add(new Group("debug group",-1,grID,-1,-1));
        List<Semester> sl = new ArrayList<>();
        sl.add(new Semester(sem,"Весенний семестр 2018-2019"));
        parseExams(gl,sl,null);
    }
    private void parseOneExam(Document doc,String grp,String sem,Integer grID,Integer numof,Integer count,Integer semiD){
        if(doc.select("#ExamTable").size()==0){
            System.out.println("[WARN][EXAMPARSE] Fail parse exam for group: "+grp+" in sem: "+sem+" "+numof+"/"+count);
            return;
        }
        Elements lines = doc.select("#ExamTable").get(0).getElementsByTag("table").get(0).getElementsByTag("tr");
        lines.forEach(tr->{
            if(tr.getElementsByClass("ExamCons").size()==0&&tr.children().size()!=0){
                Elements td = tr.children();
                Long timex = parseDateExam(td.get(0).html(),sem);
                if(timex!=-1) {
                    exams.add( genExam(td.get(1).html(),timex,grID,semiD));
                }
            }
        });
        System.out.println("[INFO][EXAMPARSE] Sucsesful parse exam for group: "+grp+" in sem: "+sem+" "+numof+"/"+count);
    }



    private Exam genExam(String code,Long timex,Integer g,Integer semID){
        Pattern patternWithTeacher = Pattern.compile("<br><p><font class=\"font-subject\">(.*?)</font><br>" +
                "<font class=\"font-classroom\"><a href=\"#\" onclick=\"return false\" onmouseup=\"GoToTheLink(.*?)\">(.*?)</a></font>" +
                "<font class=\"font-teacher\"></font></p><p><font class=\"font-teacher\">(.*?)</font></p>" +
                "<font class=\"font-teacher\"><a href=\"#\" onclick=\"return false\" onmouseup=\"GoToTheLink(.*?)\">(.*?)</a><br></font>");

        Pattern patternWithoutTeacher = Pattern.compile("<br><p><font class=\"font-subject\">(.*?)</font><br>" +
                "<font class=\"font-classroom\"><a href=\"#\" onclick=\"return false\" onmouseup=\"GoToTheLink(.*?)\">(.*?)</a></font>" +
                "<font class=\"font-teacher\"></font></p><p><font class=\"font-teacher\">(.*?)</font></p>");
        Matcher matcherT = patternWithTeacher.matcher(code.replaceAll("\n",""));
        Matcher matcherNT = patternWithoutTeacher.matcher(code.replaceAll("\n",""));
        Integer[] gID = new Integer[1];
        gID[0] = g;
        if(matcherT.matches()){
            Exam.typeOfExam t = (matcherT.group(4).equals("Консультация"))? Exam.typeOfExam.CONSULTATION: Exam.typeOfExam.EXAM;
            return new Exam(-1,t,matcherT.group(1),gID,getID(matcherT.group(5)), getID(matcherT.group(2)),timex,semID);
        }
        else if(matcherNT.matches()){
            Exam.typeOfExam t = (matcherNT.group(4).equals("Консультация"))? Exam.typeOfExam.CONSULTATION: Exam.typeOfExam.EXAM;
            return new Exam(-1,t,matcherNT.group(1),gID,null, getID(matcherNT.group(2)),timex,semID);
        }
        return null;

    }





    private long parseDateExam(String code,String semTitle){
        Pattern pattern = Pattern.compile("<p><font class=\"font-subject\">(.*?)</font><br>(.*?) (.*?) (.*?)</p>");
        Matcher matcher = pattern.matcher(code.replaceAll("\n",""));
        if(!matcher.matches())
            return -1;
        String time = matcher.group(1);
        String date = matcher.group(2)+ "."+ matcher.group(3);
        String[] preParse = semTitle.split(" ");
        String year = preParse[preParse.length-1].split("-")[1];
        try {
            return date(time,date,year);
        } catch (ParseException e) {
            return  -1;
        }
    }
    private long date(String time, String date,String year) throws ParseException {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MMM.yyyy HH:mm", new Locale("ru"));
        cal.setTime((sdf.parse(date+"."+year+" "+time)));
        return cal.getTimeInMillis();
    }
}
