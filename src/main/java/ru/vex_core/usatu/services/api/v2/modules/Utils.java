package ru.vex_core.usatu.services.api.v2.modules;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.vex_core.usatu.dao.mapper.SemesterMapper;
import ru.vex_core.usatu.models.pub.Semester;

import java.util.Date;
import java.util.List;
@Service
public class Utils {

    @Autowired
    SemesterMapper semesterMapper;

    public List<Semester> getSemesters(){
        return semesterMapper.getAllSemesters();
    }
    long MILLIS_IN_DAY = 1000*60*60*24;
    long MILLIS_IN_WEEK = MILLIS_IN_DAY*7;
    public Integer weekCalc(){
        Date date = new Date();
        Date tempDate = new Date(date.getYear(),date.getMonth()>=8 ? 8 : 1,1);
        tempDate = new Date(tempDate.getTime()+MILLIS_IN_DAY*((8-tempDate.getDay())%7));

        Integer mbweek = (int)Math.floor((date.getTime()-tempDate.getTime()) / MILLIS_IN_WEEK)+1;
        mbweek = (mbweek>25)?1:mbweek;
        return mbweek;
    }
}
