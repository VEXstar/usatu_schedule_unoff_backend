package ru.vex_core.usatu.services.api.v2.modules;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.vex_core.usatu.dao.mapper.GroupMapper;
import ru.vex_core.usatu.models.pub.Group;
import ru.vex_core.usatu.models.pub.Schedule;
import ru.vex_core.usatu.models.pub.Student;

import java.util.List;
import java.util.Map;

@Service
public class GroupModule {
    @Autowired
    GroupMapper groupMapper;


    public List<Group> getGroupsBy(Map<String,Object> params){
        return groupMapper.getGroupsBy(params);

    }
    public List<Group> getGroupByStudTerm(Map<String,Object> params){
        return groupMapper.getgetGroupsByStdTerm(params);
    }


}
