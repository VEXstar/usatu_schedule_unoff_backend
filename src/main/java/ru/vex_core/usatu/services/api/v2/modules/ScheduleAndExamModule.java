package ru.vex_core.usatu.services.api.v2.modules;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.vex_core.usatu.dao.mapper.ExamMapper;
import ru.vex_core.usatu.dao.mapper.ScheduleMapper;

import java.util.List;
import java.util.Map;

@Service
public class ScheduleAndExamModule {
    @Autowired
    ExamMapper examMapper;
    @Autowired
    ScheduleMapper scheduleMapper;
    public enum type {Schedule,Exam}
    public List<?> getIs(type t, Map<String,Object> params){
        if(t == type.Exam)
            return examMapper.getExamsBy(params);
        if(t == type.Schedule)
            return scheduleMapper.getSchedulesBy(params);
        return null;
    }
}
