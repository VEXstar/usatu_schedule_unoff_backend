package ru.vex_core.usatu.services.api.v2.modules;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.vex_core.usatu.dao.mapper.AuditoryMapper;
import ru.vex_core.usatu.models.pub.Auditory;

import java.util.List;
import java.util.Map;

@Service
public class AuditoryModule {
    @Autowired
    AuditoryMapper auditoryMapper;

    public List<Auditory> getAuditoriesBy(Map<String,Object> params){
        return auditoryMapper.getAuditoriesBy(params);
    }
}
