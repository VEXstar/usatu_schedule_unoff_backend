package ru.vex_core.usatu.services.api.v2.modules;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.vex_core.usatu.dao.mapper.AuditoryMapper;
import ru.vex_core.usatu.dao.mapper.BuildMapper;
import ru.vex_core.usatu.dao.mapper.DepartamentMapper;
import ru.vex_core.usatu.dao.mapper.FacultyMapper;
import ru.vex_core.usatu.models.pub.Build;
import ru.vex_core.usatu.models.pub.Department;
import ru.vex_core.usatu.models.pub.Faculty;

import java.util.List;
import java.util.Map;

@Service
public class HouseModule {
    @Autowired
    BuildMapper buildMapper;
    @Autowired
    FacultyMapper facultyMapper;

    @Autowired
    AuditoryMapper auditoryMapper;

    @Autowired
    DepartamentMapper departamentMapper;

    public List<Department> getDepartmentsBy(Map<String,Object> params){
        return departamentMapper.getDepartamentsBy(params);
    }
    public List<Build> getBuildsBy(Map<String,Object> params){
        return buildMapper.getBuildsBy(params);
    }
    public List<Faculty> getFacultitesBy(Map<String,Object> params){
        return facultyMapper.getFacultiesBy(params);
    }

}
