package ru.vex_core.usatu.services.api.v2;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import ru.vex_core.usatu.dao.mapper.*;
import ru.vex_core.usatu.models.pub.*;
import ru.vex_core.usatu.models.response.*;
import ru.vex_core.usatu.providers.WebRequsetUSATUProvider;
import ru.vex_core.usatu.services.api.v2.modules.*;
import ru.vex_core.usatu.services.grabling.ScheuldeGrablingService;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
public class DataStorageServiceV2 {




    @Autowired
    Utils utils;
    @Autowired
    GroupModule groupModule;
    @Autowired
    ScheduleAndExamModule scheduleAndExamModule;
    @Autowired
    TeacherModule teacherModule;
    @Autowired
    HouseModule houseModule;
    @Autowired
    StudentModule studentModule;
    @Autowired
    AuditoryModule auditoryModule;

    public ResponseEntity getAuditories(Integer id,Integer floor,Integer buildId,String term){
        Long start = System.currentTimeMillis();
        Map<String,Object> params = new HashMap<>();
        List<Auditory> auditories = null;
        if(id!=null){
            params.put("id",id);
            auditories = auditoryModule.getAuditoriesBy(params);
            return genResponseEntity(auditories, System.currentTimeMillis()-start,false);
        }
        if(floor!=null)params.put("floor",floor);
        if(buildId!=null)params.put("build_id",buildId);
        if(term!=null)params.put("name",term);
        if(params.isEmpty())
            genResponseEntity(null, System.currentTimeMillis()-start,true);
        auditories = auditoryModule.getAuditoriesBy(params);
        return genResponseEntity(auditories, System.currentTimeMillis()-start,true);
    }
    public ResponseEntity getRaspByAudID(Integer aud, Integer sem){
        Long start = System.currentTimeMillis();
        if(aud==null||sem==null){
            return genResponseEntity(null, System.currentTimeMillis()-start,false);
        }
        Map<String,Object> params = new HashMap<>();
        params.put("auditory_id",aud);
        params.put("sem_id",sem);
        return genResponseEntity(scheduleAndExamModule.getIs(ScheduleAndExamModule.type.Schedule,params),System.currentTimeMillis()-start,true);

    }
    public ResponseEntity getExamByAudID(Integer aud, Integer sem){
        Long start = System.currentTimeMillis();
        if(aud==null&&sem==null){
            return genResponseEntity(null, System.currentTimeMillis()-start,false);
        }
        Map<String,Object> params = new HashMap<>();
        params.put("auditories_id",aud);
        params.put("sem_id",sem);
        return genResponseEntity(scheduleAndExamModule.getIs(ScheduleAndExamModule.type.Exam,params),System.currentTimeMillis()-start,true);

    }



    public ResponseEntity getRaspByGroupID(Integer groupId, Integer sem){
        Long start = System.currentTimeMillis();
        if(groupId==null||sem==null){
            return genResponseEntity(null, System.currentTimeMillis()-start,false);
        }
        Map<String,Object> params = new HashMap<>();
        params.put("groups_id",new Integer[]{groupId});
        params.put("sem_id",sem);
        return genResponseEntity(scheduleAndExamModule.getIs(ScheduleAndExamModule.type.Schedule,params),System.currentTimeMillis()-start,true);

    }
    public ResponseEntity getRaspByTeacherID(Integer teacherID, Integer sem){
        Long start = System.currentTimeMillis();
        if(teacherID==null||sem==null){
            return genResponseEntity(null, System.currentTimeMillis()-start,false);
        }
        Map<String,Object> params = new HashMap<>();
        params.put("teacher_id",teacherID);
        params.put("sem_id",sem);
        return genResponseEntity(scheduleAndExamModule.getIs(ScheduleAndExamModule.type.Schedule,params),System.currentTimeMillis()-start,true);

    }


    public ResponseEntity getExamByGroupID(Integer groupId, Integer sem){
        Long start = System.currentTimeMillis();
        if(groupId==null||sem==null){
            return genResponseEntity(null, System.currentTimeMillis()-start,false);
        }
        Map<String,Object> params = new HashMap<>();
        params.put("groups_id",new Integer[]{groupId});
        params.put("sem_id",sem);
        return genResponseEntity(scheduleAndExamModule.getIs(ScheduleAndExamModule.type.Exam,params),System.currentTimeMillis()-start,true);

    }
    public ResponseEntity getExamByTeacherID(Integer teacherID, Integer sem){
        Long start = System.currentTimeMillis();
        if(teacherID==null||sem==null){
            return genResponseEntity(null, System.currentTimeMillis()-start,false);
        }
        Map<String,Object> params = new HashMap<>();
        params.put("teacher_id",teacherID);
        params.put("sem_id",sem);
        return genResponseEntity(scheduleAndExamModule.getIs(ScheduleAndExamModule.type.Exam,params),System.currentTimeMillis()-start,true);

    }



    public ResponseEntity getWeekByCalc(){
        Long start = System.currentTimeMillis();
        WeekResponseModel weekResponseModel = new WeekResponseModel(utils.weekCalc());
        List<WeekResponseModel> weekResponseModels = new ArrayList<>();
        weekResponseModels.add(weekResponseModel);
        return genResponseEntity(weekResponseModels, System.currentTimeMillis()-start,false);
    }

    public ResponseEntity findGroupBy(Integer id,String term,Integer lvl,Integer facID,Integer student,String stdterm){
        Long start = System.currentTimeMillis();
        List<Group> grps = null;
        Map<String,Object> params = new HashMap<>();
        if(stdterm!=null){
            params.put("term",stdterm);
            grps = groupModule.getGroupByStudTerm(params);
            sortGroup(grps);
            return genResponseEntity(grps ,System.currentTimeMillis()-start,true);
        }
        if(student!=null){
            params.put("sid",student);
            grps = groupModule.getGroupByStudTerm(params);
            sortGroup(grps);
            return genResponseEntity(grps ,System.currentTimeMillis()-start,false);
        }
        if(id!=null){
            params.put("id",id);
            grps =  groupModule.getGroupsBy(params);
            return genResponseEntity(grps,System.currentTimeMillis()-start,false);
        }
        if(term!=null)params.put("term",term);
        if(lvl!=null)params.put("level",lvl);
        if(facID!=null)params.put("fac_id",facID);
        if(params.isEmpty())
            genResponseEntity(null, System.currentTimeMillis()-start,true);
        grps = groupModule.getGroupsBy(params);
        return genResponseEntity(grps ,System.currentTimeMillis()-start,true);
    }


    public ResponseEntity findTeacherBy(Integer id,String term,Integer dep,Integer fac){
        Long start = System.currentTimeMillis();
        Map<String,Object> params = new HashMap<>();
        List<Teacher> teachers = null;
        if(id!=null){
            params.put("id",id);
            teachers = teacherModule.getTeacherBy(params);
            return genResponseEntity(teachers, System.currentTimeMillis()-start,false);
        }
        if(term!=null)params.put("term",term);
        if(dep!=null)params.put("dep_id",dep);
        if(fac!=null)params.put("fac_id",fac);
        if(params.isEmpty())
            genResponseEntity(null, System.currentTimeMillis()-start,true);
        teachers = teacherModule.getTeacherBy(params);
        return genResponseEntity(teachers,System.currentTimeMillis()-start,true);
    }

    public ResponseEntity findDepartmentByFacId(Integer fac){
        Long start = System.currentTimeMillis();
        Map<String,Object> params = new HashMap<>();
        params.put("fac_id",fac);
        if(params.isEmpty())
            genResponseEntity(null, System.currentTimeMillis()-start,true);
        List<Department> departaments = houseModule.getDepartmentsBy(params);
        return genResponseEntity(departaments, System.currentTimeMillis()-start,true);
    }
    public ResponseEntity getBuilds(){
        Long start = System.currentTimeMillis();
        List<Build> builds = houseModule.getBuildsBy(new HashMap<>());
        return genResponseEntity(builds,System.currentTimeMillis()-start,true);
    }

    public ResponseEntity getFaculties(){
        Long start = System.currentTimeMillis();
        List<Faculty> faculties = houseModule.getFacultitesBy(new HashMap<>());
        return genResponseEntity(faculties,System.currentTimeMillis()-start,true);
    }

    public ResponseEntity getSemesters(){
        Long start = System.currentTimeMillis();
        List<Semester> semesters = utils.getSemesters();
        return genResponseEntity(semesters, System.currentTimeMillis()-start,true);
    }

    public ResponseEntity findStudentBy(String term,Integer groupID,Integer studId){
        Long start = System.currentTimeMillis();
        Map<String,Object> params = new HashMap<>();
        List<Student> students = null;
        if(studId!=null){
            params.put("id",studId);
            students = studentModule.getStudentBy(params);
            return genResponseEntity(students, System.currentTimeMillis()-start,false);
        }
        if(groupID!=null)
            params.put("group_id",groupID);
        if(term!=null)
            params.put("term",term);
        if(params.isEmpty())
            return genResponseEntity(null,System.currentTimeMillis()-start,false);
        students = studentModule.getStudentBy(params);
        return genResponseEntity(students, System.currentTimeMillis()-start,true);

    }

    Gson gson = new Gson();
    private ResponseEntity genResponseEntity(List<?> data,Long totaltimeReq,boolean islist){
        UniversalResponseModelForV2Api universalResponseModelForV2Api = null;
        if(data==null&&!islist){
            universalResponseModelForV2Api = new UniversalResponseModelForV2Api("Error",UniversalResponseModelForV2Api.responseCodes.NO_MATCH_FIELDS,null,totaltimeReq,System.currentTimeMillis());
        }
        else if(!islist&&data!=null){
            universalResponseModelForV2Api = new UniversalResponseModelForV2Api(null,UniversalResponseModelForV2Api.responseCodes.OK,data.get(0),totaltimeReq,System.currentTimeMillis());
        }
        else if(islist&&data!=null){
            universalResponseModelForV2Api = new UniversalResponseModelForV2Api(null,UniversalResponseModelForV2Api.responseCodes.OK,data,totaltimeReq,System.currentTimeMillis());
        }
        else {
            universalResponseModelForV2Api = new UniversalResponseModelForV2Api("Server Error",UniversalResponseModelForV2Api.responseCodes.SERVER_ERROR,null,totaltimeReq,System.currentTimeMillis());
        }
        if(universalResponseModelForV2Api.getCode() == UniversalResponseModelForV2Api.responseCodes.OK){
            return ResponseEntity.ok(gson.toJson(universalResponseModelForV2Api));
        }
        if(universalResponseModelForV2Api.getCode()  == UniversalResponseModelForV2Api.responseCodes.NO_MATCH_FIELDS){
            return ResponseEntity.badRequest().body(gson.toJson(universalResponseModelForV2Api));
        }
        if(universalResponseModelForV2Api.getCode() == UniversalResponseModelForV2Api.responseCodes.FAIL_PARSE||universalResponseModelForV2Api.getCode() == UniversalResponseModelForV2Api.responseCodes.SERVER_ERROR){
            return ResponseEntity.status(500).body(gson.toJson(universalResponseModelForV2Api));
        }
        return ResponseEntity.notFound().build();
    }

    private void sortGroup(List<Group> inpt){
        inpt.sort(Comparator.comparingInt(Group::getLevel));
    }
}
