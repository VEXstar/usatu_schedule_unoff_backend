package ru.vex_core.usatu.services.api.v2.modules;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.vex_core.usatu.dao.mapper.TeacherMapper;
import ru.vex_core.usatu.models.pub.Teacher;

import java.util.List;
import java.util.Map;

@Service
public class TeacherModule {
    @Autowired
    TeacherMapper teacherMapper;

    public List<Teacher> getTeacherBy(Map<String,Object> params){
        return teacherMapper.getTeachersBy(params);
    }

}
