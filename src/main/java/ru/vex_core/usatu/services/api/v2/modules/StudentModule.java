package ru.vex_core.usatu.services.api.v2.modules;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.vex_core.usatu.dao.mapper.StudentMapper;
import ru.vex_core.usatu.models.pub.Student;

import java.util.List;
import java.util.Map;

@Service
public class StudentModule {
    @Autowired
    StudentMapper studentMapper;

    public List<Student> getStudentBy(Map<String,Object> params){
        return studentMapper.getStudentsBy(params);
    }
}
