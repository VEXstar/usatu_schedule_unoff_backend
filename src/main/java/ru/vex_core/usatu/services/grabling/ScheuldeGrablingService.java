package ru.vex_core.usatu.services.grabling;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.vex_core.usatu.dao.mapper.ExamMapper;
import ru.vex_core.usatu.dao.mapper.LastUpdateMapper;
import ru.vex_core.usatu.dao.mapper.ScheduleMapper;
import ru.vex_core.usatu.models.pub.*;
import ru.vex_core.usatu.logic.parse.scheulde.ExamParser;
import ru.vex_core.usatu.logic.parse.scheulde.ScheuldeParser;
import ru.vex_core.usatu.providers.WebRequsetUSATUProvider;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.stream.Collectors;
@Service
@PropertySource("classpath:usatu.properties")
public class ScheuldeGrablingService {

    private WebRequsetUSATUProvider provider;

    @Value("${usatu.login}")
    private String login;
    @Value("${usatu.password}")
    private String password;

    @Autowired
    ScheduleMapper scheduleMapper;
    @Autowired
    ExamMapper examMapper;
    @Autowired
    LastUpdateMapper lastUpdateMapper;

    @PostConstruct
    public void InitData(){
        provider = new WebRequsetUSATUProvider(login,password, "lk.ugatu.su",this.getClass().getName());
    }


    public void grabTable(List<Group> group, List<Semester> semester, boolean force, Debug debug){


        List<Long> lu = lastUpdateMapper.getLastUpdate("schedupdtime");
        if(!force&&(lu.size()!=0&&(System.currentTimeMillis()-lu.get(lu.size()-1))<1000*60*60*24*3))
            return;


        Long start = System.currentTimeMillis();
        System.out.println("[INFO][SCHEDULE GRABLING] <--------------------------------- START --------------------------------->");

        Semester filteredSemseter = semester.stream().max(Comparator.comparingInt(Semester::getId)).get();



        ScheuldeParser scheuldeParser;
        if(scheduleMapper.getRows()>0){
            List<Semester> fsl = new ArrayList<>();
            fsl.add(filteredSemseter);
            Map<String,Object> params = new HashMap<>();
            params.put("sem_id",filteredSemseter.getId());
            List<Integer> schedules = scheduleMapper.getSchedulesBy(params).stream().map(v->v.getId()).collect(Collectors.toList());
            scheuldeParser = new ScheuldeParser(provider,"https://lk.ugatu.su/raspisanie/",group,fsl,debug);
            if(schedules.size()>0)
                scheduleMapper.removeSchedulesByList(schedules);
        }
        else {
            scheuldeParser = new ScheuldeParser(provider,"https://lk.ugatu.su/raspisanie/",group,semester,debug);
        }
        List<Schedule> scheuldeParserSchedules = scheuldeParser.getSchedules();
        if(scheuldeParserSchedules.size()>0)
            pushSchedule(scheuldeParserSchedules);

        ExamParser examParser;
        if(examMapper.getRows()>0){
            List<Semester> fsl = new ArrayList<>();
            fsl.add(filteredSemseter);
            Map<String,Object> params = new HashMap<>();
            params.put("sem_id",filteredSemseter.getId());
            List<Integer> exams = examMapper.getExamsBy(params).stream().map(v->v.getId()).collect(Collectors.toList());
            examParser = new ExamParser(provider,"https://lk.ugatu.su/raspisanie/",group,fsl,debug);
            if(exams.size()>0)
                examMapper.removeExamsBy(exams);
        }
        else {
            examParser = new ExamParser(provider,"https://lk.ugatu.su/raspisanie/",group,semester,debug);
        }
        List<Exam> examParserExams = examParser.getExams();
        if(examParserExams.size()>0)
            pushExam(examParserExams);
        Long end = (System.currentTimeMillis() - start)/60000;
        System.out.println("[INFO][SCHEDULE GRABLING] <--------------------------------- END ---------------------------------> \n Total time parse (in mins) :"+ end);
        lastUpdateMapper.updateTimeStamp(System.currentTimeMillis(),"schedupdtime");
        debug.setStatus("All updated");
    }

    private void pushExam(List<Exam> exams){
        Thread thread = new Thread(()->{
            boolean err = true;
            while (err){
                try{
                    List<Exam> temp = new ArrayList<>();
                    for(int i =0;i<exams.size();i++){
                        temp.add(exams.get(i));
                        if(i!=0&&i%500==0){
                            try { Thread.sleep(2000); } catch (InterruptedException e) { }
                            examMapper.addAllExams(temp);
                            temp.clear();
                        }
                    }
                    if(temp.size()>0)
                        examMapper.addAllExams(temp);
                    err = false;
                }
                catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        thread.start();
    }

    private void pushSchedule(List<Schedule> schedules){

        Thread thread = new Thread(()->{
            boolean err = true;
            while (err){
                try{
                    List<Schedule> temp = new ArrayList<>();
                    for(int i =0;i<schedules.size();i++){
                        temp.add(schedules.get(i));
                        if(i!=0&&i%1000==0){
                            try { Thread.sleep(500); } catch (InterruptedException e) { }
                            scheduleMapper.addAllSchedules(temp);
                            temp.clear();
                        }
                    }
                    if(temp.size()>0)
                        scheduleMapper.addAllSchedules(temp);
                    err = false;
                }
                catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        thread.start();
    }

    //TODO: автоматическое обновление

}
