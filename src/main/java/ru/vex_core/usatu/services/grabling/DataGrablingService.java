package ru.vex_core.usatu.services.grabling;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.vex_core.usatu.dao.mapper.*;
import ru.vex_core.usatu.logic.parse.data.BuildingParser;
import ru.vex_core.usatu.logic.parse.data.HumanParser;
import ru.vex_core.usatu.models.pub.*;
import ru.vex_core.usatu.providers.WebRequsetUSATUProvider;

import javax.annotation.PostConstruct;
import java.util.*;


@Service
@PropertySource("classpath:usatu.properties")
public class DataGrablingService {


    @Value("${usatu.login}")
    private String login;
    @Value("${usatu.password}")
    private String password;


    @Autowired
    ScheuldeGrablingService scheuldeGrablingService;

    @Autowired
    AuditoryMapper auditoryMapper;
    @Autowired
    LastUpdateMapper lastUpdateMapper;
    @Autowired
    BuildMapper buildMapper;
    @Autowired
    DepartamentMapper departamentMapper;
    @Autowired
    FacultyMapper facultyMapper;
    @Autowired
    GroupMapper groupMapper;
    @Autowired
    SemesterMapper semesterMapper;
    @Autowired
    StudentMapper studentMapper;
    @Autowired
    TeacherMapper teacherMapper;

    private Debug updateStatus;

    public Debug getUpdateStatus() {
        return updateStatus;
    }
    public Map<String,Long> getLastUpdates(){
        List<Long> lu = lastUpdateMapper.getLastUpdate("dataupdtime");
        List<Long> lu1 = lastUpdateMapper.getLastUpdate("schedupdtime");
        Map<String,Long> upts = new HashMap<>();
        upts.put("Data",lu.get(0));
        upts.put("Schedule",lu1.get(0));
        return upts;
    }

    private WebRequsetUSATUProvider provider;

    private void autoUpdateStart(){
        long timeUpdate = 14*60*60*10000;
        Timer timer = new Timer();
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                try { updateData(false); }
                catch (InterruptedException e) { e.printStackTrace(); }
            }
        };
        timer.scheduleAtFixedRate(task,timeUpdate,timeUpdate);
    }

    @PostConstruct
    public void InitData(){
        updateStatus  = new Debug();
        updateStatus.setStatus("All updated");
            provider = new WebRequsetUSATUProvider(login,password,"lk.ugatu.su",this.getClass().getName());
            Thread thread = new Thread(()->{
                try {
                    updateData(false);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
            thread.start();
        autoUpdateStart();
    }

    public void updateData(boolean force) throws InterruptedException {
        if(false){
            System.out.println("YOU IN DEBUG MODE UPDATE DISABLED!!!!");
            return;
        }
        List<Long> lu = lastUpdateMapper.getLastUpdate("dataupdtime");
        if(!force&&(lu.size()!=0&&(System.currentTimeMillis()-lu.get(lu.size()-1))<1000*60*60*24*7)){
            scheuldeGrablingService.grabTable(groupMapper.getGroupsBy(new HashMap<>()),semesterMapper.getAllSemesters(),force,updateStatus);
            return;
        }
        updateStatus.setStatus("updating data...");
        lastUpdateMapper.updateTimeStamp(System.currentTimeMillis(),"dataupdtime");
        Long start = System.currentTimeMillis();
        System.out.println("[INFO][preDATA GRABLING] <--------------------------------- START ---------------------------------> \n absolute time: "+start);
        HumanParser hParse = new HumanParser(provider,"https://lk.ugatu.su/SendMessage","https://lk.ugatu.su/teacher/");
        BuildingParser bParse = new BuildingParser(provider,"https://lk.ugatu.su/audience/");
        Long end = System.currentTimeMillis() - start;
        System.out.println("[INFO][preDATA GRABLING] <--------------------------------- END ---------------------------------> \n absolute time: "+System.currentTimeMillis()+" local time: "+end);
        checkUpdatesAndUpdate(bParse.getAuditories(),bParse.getBuilds(),hParse.getDepartments(),hParse.getFacs(),hParse.getGroups(),bParse.getSemesters(),hParse.getStudents(),hParse.getTeachers());
        updateStatus.setStatus("updating schedule...");
        scheuldeGrablingService.grabTable(hParse.getGroups(),bParse.getSemesters(),force,updateStatus);
        updateStatus.setStatus("All updated");

    }





    private void checkUpdatesAndUpdate(List<Auditory> auditories, List<Build> builds, List<Department> departments, List<Faculty> faculties, List<Group> groups,List<Semester> semesters,List<Student> students,List<Teacher> teachers){

        if(auditoryMapper.getRows()>0)
            auditoryMapper.removeAuditoriesBy(new HashMap<>());
        auditoryMapper.addAllAuditories(auditories);

        if(buildMapper.getRows()>0)
            buildMapper.removeBuildsBy(new HashMap<>());
        buildMapper.addAllBuilds(builds);

        if(departamentMapper.getRows()>0)
            departamentMapper.cleanDepartaments();
        departamentMapper.addAllDepartaments(departments);

        if(facultyMapper.getRows()>0)
            facultyMapper.cleanFacs();
        facultyMapper.addAllFacs(faculties);

        if(groupMapper.getRows()>0)
            groupMapper.removeGroupsBy(new HashMap<>());
        groupMapper.addAllGroups(groups);

        if(semesterMapper.getRows()>0)
            semesterMapper.cleanSemesters();
        semesterMapper.addAll(semesters);

        if(studentMapper.getRows()>0)
            studentMapper.removeStudentsBy(new HashMap<>());
        List<Student> temp = new ArrayList<>();
        for(int i = 0; i<students.size();i++){
            temp.add(students.get(i));
           if(i!=0&&i%2000==0){

               studentMapper.addAllStudents(temp);
               temp.clear();
           }
        }
        if(temp.size()>0)
            studentMapper.addAllStudents(temp);

        if(teacherMapper.getRows()>0)
            teacherMapper.removeTeachersBy(new HashMap<>());

        List<Teacher> temp1 = new ArrayList<>();
        for(int i = 0; i<teachers.size();i++){
            temp1.add(teachers.get(i));
            if(i!=0&&i%2000==0){

                teacherMapper.addAllTeacher(temp1);
                temp1.clear();
            }
        }
        if(temp1.size()>0)
            teacherMapper.addAllTeacher(temp1);
        System.out.println("[INFO][preDATA GRABLING] data push to db sucsesful");


    }
}
